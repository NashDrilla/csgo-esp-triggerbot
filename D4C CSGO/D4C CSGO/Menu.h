#pragma once
#include <Windows.h>
#include <string>
#include "Render.h"
#include "XOR.h"

#include <detours.h>
#pragma comment(lib, "detours.lib")
#include <d3dx9.h>
#pragma comment(lib, "d3dx9.lib")

struct Buttons
{
	const char* caption;
	int      x;
	int      y;
	int      w;
	int      h;
	bool     clicked;
	bool     mOver;
	bool	 stat;
};

extern Buttons Button[20];

namespace Menu
{
	extern void AddButton(const char* bcaption, int bx, int by, int bw, int bh, bool status);

	extern void DrawButton(LPDIRECT3DDEVICE9 Dev);

	extern void InitMenu(LPDIRECT3DDEVICE9 Dev);
}