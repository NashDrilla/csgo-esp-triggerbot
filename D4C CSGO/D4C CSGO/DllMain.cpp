#include <Windows.h>
#include "Memory.h"
#include <string>
#include "Render.h"
#include "Netvars.h"
#include "XOR.h"
#include "Menu.h"
#include "W2S.h"

#include <detours.h>
#pragma comment(lib, "detours.lib")

#include <d3dx9.h>
#pragma comment(lib, "d3dx9.lib")

using namespace hazedumper;
using namespace signatures;
using namespace netvars;

using namespace W2S;

HRESULT(APIENTRY* oEndScene)(LPDIRECT3DDEVICE9 pDevice);


D3DCOLOR fontlightGrey = D3DCOLOR_ARGB(255, 211, 211, 211);
D3DCOLOR fontWhite = D3DCOLOR_ARGB(255, 255, 255, 255);
D3DCOLOR fontblack = D3DCOLOR_ARGB(255, 0, 0, 0);

HRESULT APIENTRY hook_EndScene(LPDIRECT3DDEVICE9 pDevice)
{
	Menu::InitMenu(pDevice);

	if (Button[7].stat)
	{
		Render.FilledCircle(pDevice, GetSystemMetrics(SM_CXSCREEN) / 2, GetSystemMetrics(SM_CYSCREEN) / 2, 5, 10, D3DCOLOR_ARGB(255, 255, 255, 255)); //Crosshair
	}

	DWORD Client = Memory.GetModule(XOR("client.dll"));

	view_matrix Matrix;

	memcpy(&Matrix, (PBYTE*)(Client + dwViewMatrix), sizeof(Matrix));

	DWORD LocalPlayer = *(DWORD*)(Client + dwLocalPlayer);

	if (LocalPlayer == NULL)
	{
		while (LocalPlayer == NULL)
		{
			LocalPlayer = *(DWORD*)(Client + dwLocalPlayer);
		}
	}

	int myTeam = *(int*)(LocalPlayer + m_iTeamNum);

	//our entity list loop
	for (int i = 0; i < 32; i++)
	{
		//each entity is 0x10 apart in memory
		DWORD entity = *(DWORD*)(Client + dwEntityList + (i * 0x10));

		if (entity != NULL)
		{
			if (entity != LocalPlayer)
			{
				int entityTeam = *(int*)(entity + m_iTeamNum);

				D3DXVECTOR3 entityLocation = *(D3DXVECTOR3*)(entity + m_vecOrigin);

				DWORD dwBoneMatrix = *(DWORD*)(entity + m_dwBoneMatrix);

				int health = *(int*)(entity + m_iHealth);
				int isDormant = *(int*)(entity + m_bDormant);


				if (isDormant == 0 && health > 0)//Checks if the entity is culled
				{
					//this gets the x,y,z of the head bone
					float enemyHeadX = *(float*)(dwBoneMatrix + 0x30 * 8 + 0x0C);
					float enemyHeadY = *(float*)(dwBoneMatrix + 0x30 * 8 + 0x1C);
					float enemyHeadZ = *(float*)(dwBoneMatrix + 0x30 * 8 + 0x2C);
					//turns the head bone floats into a Vec3
					D3DXVECTOR3 enemyHeadPos = { enemyHeadX, enemyHeadY, enemyHeadZ };

					enemyHeadPos.z += 8;
					D3DXVECTOR2 EntPosition = WorldToScreen(entityLocation, Matrix);
					D3DXVECTOR2 EntHead = WorldToScreen(enemyHeadPos, Matrix);

					int height = EntPosition.y - EntHead.y;
					int width = height / 2;

					float healthPcnt = health / 100.f;
					int HealthHeight = width * healthPcnt;


					D3DCOLOR TeamCol = D3DCOLOR_ARGB(255, 0, 255, 0);
					D3DCOLOR EnemyCol = D3DCOLOR_ARGB(255, 255, 0, 0);

					if (myTeam != entityTeam)
					{
						if (Button[0].stat)
						{
							Render.DrawEdgeRectangleBox(pDevice, EntPosition, EntHead, 2, EnemyCol);
							Render.FilledBox(pDevice, EntPosition.x - width / 2, EntPosition.y - height, width, height, D3DCOLOR_ARGB(50, 0, 0, 0));
						}

						if (Button[1].stat)
						{
							Render.FilledBox(pDevice, EntPosition.x - width / 2, EntPosition.y + 1, HealthHeight, 2, D3DCOLOR_ARGB(255, 50, 205, 50));
						}

						if (Button[2].stat)
						{
							char HealthChar[255];
							XOR(sprintf_s(HealthChar, sizeof(HealthChar), "HP: %i", (int)health));
							Render.DrawShadowText(pDevice, HealthChar, EntPosition.x - 20, EntPosition.y + 4, EnemyCol);
						}
					}

					//If my team
					else
					{
						if (Button[3].stat)
						{
							Render.DrawEdgeRectangleBox(pDevice, EntPosition, EntHead, 2, D3DCOLOR_ARGB(255, 0, 191, 255));
							Render.FilledBox(pDevice, EntPosition.x - width / 2, EntPosition.y - height, width, height, D3DCOLOR_ARGB(50, 0, 0, 0));
						}

						if (Button[4].stat)
						{
							Render.FilledBox(pDevice, EntPosition.x - width / 2, EntPosition.y + 1, HealthHeight, 2, TeamCol);
						}

						if (Button[5].stat)
						{
							char HealthChar[255];
							XOR(sprintf_s(HealthChar, sizeof(HealthChar), "HP: %i", (int)health));
							Render.DrawShadowText(pDevice, HealthChar, EntPosition.x - 20, EntPosition.y + 4, EnemyCol);
						}
					}
				}
			}
		}
	}
	
	return oEndScene(pDevice);
}


struct vec3
{
	float x, y, z;
};

void TriggerBot()
{
	DWORD ClientModule = Memory.GetModule(XOR("client.dll"));

	DWORD LocalPlayer = *(DWORD*)(ClientModule + dwLocalPlayer);

	do
	{
		LocalPlayer = *(DWORD*)(ClientModule + dwLocalPlayer);
	} while (LocalPlayer == NULL);

	int myTeam = *(int*)(LocalPlayer + m_iTeamNum);

	int crossEntity = *(int*)(LocalPlayer + m_iCrosshairId);
	DWORD entity = *(DWORD*)(ClientModule + dwEntityList + (crossEntity - 1) * 0x10);

	int enemyHealth = *(int*)(entity + m_iHealth);
	int entityTeam = *(int*)(entity + m_iTeamNum);

	if (Button[6].stat)
	{
		if (GetAsyncKeyState(VK_MENU))
		{
			if (entityTeam != myTeam  && enemyHealth > 0)
			{
				int ShootOn = 5;
				int ShootOff = 4;

				Sleep(1);
				WriteProcessMemory(GetCurrentProcess(), (LPVOID)(ClientModule + dwForceAttack), &ShootOn, sizeof(ShootOn), NULL);
				Sleep(20);
				WriteProcessMemory(GetCurrentProcess(), (LPVOID)(ClientModule + dwForceAttack), &ShootOff, sizeof(ShootOff), NULL);
				Sleep(100);

			}
		}
	}
}

DWORD WINAPI ThreadMain(LPVOID param)
{
	DWORD dwAddress = Memory.GetSignature((char*)"d3d9.dll", (char*)"6A 14 B8 ? ? ? ? E8 ? ? ? ? 8B 7D 08 8B DF 8D 47 04 F7 DB 1B DB 23 D8 89 5D E0");

	oEndScene = (HRESULT(APIENTRY*)(LPDIRECT3DDEVICE9 pDevice)) dwAddress;

	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourAttach(&(PVOID&)oEndScene, hook_EndScene);
	DetourTransactionCommit(); //Write the hook
	
	

	while (true)
	{
		TriggerBot();

		if (GetAsyncKeyState(VK_END))
		{
			break;
		}
		Sleep(1);
	}


	//Unhook
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourDetach(&(PVOID&)oEndScene, hook_EndScene);
	DetourTransactionCommit(); //Write the hook

	Sleep(1000);

	//Uninject
	FreeLibraryAndExitThread((HMODULE)param, 0);


	return 0;
}

BOOL WINAPI DllMain(HINSTANCE hMod, DWORD dwReason, LPVOID lpReserved)
{
	if (dwReason == DLL_PROCESS_ATTACH)
	{
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)&ThreadMain, hMod, 0, 0);
	}
	return TRUE;
}