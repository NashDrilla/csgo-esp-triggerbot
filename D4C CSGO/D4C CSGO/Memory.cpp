#include "Memory.h"

DWORD cMemory::dwFindPattern(DWORD dwAddress, DWORD dwLength, const char* szPattern)
{
	{
		const char* pat = szPattern;
		DWORD firstMatch = NULL;
		for (DWORD pCur = dwAddress; pCur < dwLength; pCur++)
		{
			if (!*pat) return firstMatch;
			if (*(PBYTE)pat == '\?' || *(BYTE*)pCur == getByte(pat)) {
				if (!firstMatch) firstMatch = pCur;
				if (!pat[2]) return firstMatch;
				if (*(PWORD)pat == '\?\?' || *(PBYTE)pat != '\?') pat += 3;
				else pat += 2;
			}
			else {
				pat = szPattern;
				firstMatch = 0;
			}
		}
		return NULL;
	}
}

DWORD cMemory::GetSignature(const char* szModule, char* chPattern)
{
	HMODULE hmModule = GetModuleHandle(szModule);
	PIMAGE_DOS_HEADER pDOSHeader = (PIMAGE_DOS_HEADER)hmModule;
	PIMAGE_NT_HEADERS pNTHeaders = (PIMAGE_NT_HEADERS)(((DWORD)hmModule) + pDOSHeader->e_lfanew);
	return dwFindPattern(((DWORD)hmModule) + pNTHeaders->OptionalHeader.BaseOfCode, ((DWORD)hmModule) + pNTHeaders->OptionalHeader.SizeOfCode, chPattern);
}

DWORD cMemory::GetModule(const char* moduleName)
{
	HANDLE hmodule = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, GetCurrentProcessId());
	MODULEENTRY32 mEntry;
	mEntry.dwSize = sizeof(mEntry);

	do {
		if (!strcmp(mEntry.szModule, moduleName)) {
			CloseHandle(hmodule);

			Module = { (DWORD)mEntry.hModule };
			return Module;
		}
	} while (Module32Next(hmodule, &mEntry));
}

template<typename var>
inline var cMemory::WriteMemory(DWORD Address, var Value)
{
	return WriteProcessMemory(GetCurrentProcess(), (LPVOID)Address, &Value, sizeof(var), 0);
}

template<typename var>
var cMemory::ReadMemory(DWORD Address)
{
	var value;
	ReadProcessMemory(GetCurrentProcess(), (LPCVOID)Address, &value, sizeof(var), NULL);
	return value;
}

cMemory Memory;