#pragma once
#include <Windows.h>
#include "Memory.h"
#include <string>
#include "Render.h"
#include "Netvars.h"
#include "XOR.h"
#include "Menu.h"

#include <detours.h>
#pragma comment(lib, "detours.lib")

#include <d3dx9.h>
#pragma comment(lib, "d3dx9.lib")

struct view_matrix
{
	float flMatrix[4][4];
};

namespace W2S
{
	extern D3DXVECTOR2 WorldToScreen(D3DXVECTOR3 EnemyPosition, view_matrix ViewMatrix);
}