#pragma once

#include <Windows.h>
#include <TlHelp32.h>
#include <iostream>
#include <vector>
#include <cassert>
#include <memory>
#include <sstream>

#define INRANGE(x,a,b)    (x >= a && x <= b)
#define getBits( x )    (INRANGE((x&(~0x20)),'A','F') ? ((x&(~0x20)) - 'A' + 0xa) : (INRANGE(x,'0','9') ? x - '0' : 0))
#define getByte( x )    (getBits(x[0]) << 4 | getBits(x[1]))
#define ABS(x) ((x<0) ? (-x) : (x))

class cMemory
{
public:

    DWORD dwFindPattern(DWORD dwAddress, DWORD dwLength, const char* szPattern);
    

    DWORD GetSignature(const char* szModule, char* chPattern);
    

    DWORD Module;

    DWORD GetModule(const char* moduleName);

	template <typename var>
	var WriteMemory(DWORD Address, var Value);

	// Basic RPM wrapper, easier to use.
	template <typename var>
    var ReadMemory(DWORD Address);
};
extern cMemory Memory;