#include "W2S.h"

D3DXVECTOR2 W2S::WorldToScreen(D3DXVECTOR3 EnemyPosition, view_matrix ViewMatrix)
{
	/*
	HWND hwnd = FindWindow(NULL, XOR("Counter-Strike: Global Offensive"));
	RECT rct;
	GetClientRect(hwnd, &rct);

	float Width = rct.right - rct.left;
	float Height = rct.bottom - rct.top;

	float X = rct.left;
	float Y = rct.top;
	*/

	int Width = GetSystemMetrics(SM_CXSCREEN);
	int Height = GetSystemMetrics(SM_CYSCREEN);

	int X = Width - Width;
	int Y = Height - Height;

	D3DXVECTOR2 ScreenPosition;
	if (ViewMatrix.flMatrix)
	{
		ScreenPosition.x = ViewMatrix.flMatrix[0][0] * EnemyPosition.x + ViewMatrix.flMatrix[0][1] * EnemyPosition.y + ViewMatrix.flMatrix[0][2] * EnemyPosition.z + ViewMatrix.flMatrix[0][3];
		ScreenPosition.y = ViewMatrix.flMatrix[1][0] * EnemyPosition.x + ViewMatrix.flMatrix[1][1] * EnemyPosition.y + ViewMatrix.flMatrix[1][2] * EnemyPosition.z + ViewMatrix.flMatrix[1][3];
		float w = ViewMatrix.flMatrix[3][0] * EnemyPosition.x + ViewMatrix.flMatrix[3][1] * EnemyPosition.y + ViewMatrix.flMatrix[3][2] * EnemyPosition.z + ViewMatrix.flMatrix[3][3];
		if (w > 0.01f)
		{
			float invw = 1.0f / w;
			ScreenPosition.x *= invw;
			ScreenPosition.y *= invw;
			float x = Width / 2.f;
			float y = Height / 2.f;
			x += 0.5f * ScreenPosition.x * Width + 0.5f;
			y -= 0.5f * ScreenPosition.y * Height + 0.5f;
			ScreenPosition.x = x + X;
			ScreenPosition.y = y + Y;
		}
		else
		{
			ScreenPosition.x = -1.f;
			ScreenPosition.y = -1.f;
		}
	}
	return ScreenPosition;
}